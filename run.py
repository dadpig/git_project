# -*- coding: utf-8 -*-

import pytest

# 每次运行测试用例之前先将用例记录数据归零：
open("case_num.json", "w").write('{"case_num": 0, "fail_num": 0}')
pytest.main(["调用日志框架/test.py", "--html=report/report.html", "--self-contained-html"])
# 每次运行完数据之后去获取一下执行的数据
f = open("case_num.json").read()
print("用例执行情况：", f)
