# -*- coding: utf-8 -*-
# author: 华测-长风老师

# 构建一个接口用例构造器

from requests import request
from 日志框架.sys_log import Logger


def api(protocol, host, port, method, path=None, **kwargs):
    """
    :param protocol:
    :param host:
    :param port:
    :param kwargs:
    :param method:
    :param path:
    :return:
    """
    kwargs.update({
        "url": f"{protocol}://{host}:{port}/{path}",
        "method": method,
    })
    l = Logger("api-test")
    for key in kwargs:
        l.logger.info(f"api中有：{key}, 值为：{kwargs[f'{key}']}")

    return kwargs


def test01():
    data = api("http", "shop-xo.hctestedu.com", 80, "post",
               path="index.php?s=/api/user/login&application=app",
               data={
                   "pwd": "changfeng",
                   "type": "username",
                   "accounts": "test_changfeng",
               }
               )
    print(data)
    r = request(**data)


def test02():
    data = api("http", "shop-xo.hctestedu.com", 80, "post",
               path="index.php?s=/api/user/login&application=app",
               data={
                   "pwd": "changfeng",
                   "type": "username",
                   "accounts": "test_changfeng",
               }
               )

    r = request(**data)
