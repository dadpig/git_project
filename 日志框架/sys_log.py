# -*- coding: utf-8 -*-
# author: 华测-长风老师

# 封装日志处理文件
import logging
import os.path

"""
日志一般构建好一次，就不会再变化了
"""


class Logger:

    def __init__(self, logger_name, log_dirname="logs/", log_name="/api.log"):
        self.logger = logging.getLogger(logger_name)  # 设置日志的名称；
        self.logger.setLevel(logging.DEBUG)  # 设置实例的日志等级

        # 创建一个日志对象，用于写入文件：
        log_path = os.path.dirname(log_dirname)  # 设置文件输出的路径；
        log_name = log_path + log_name  # logs/api.log

        """
        考虑编码格式，指定编码格式
        """
        try:
            fh = logging.FileHandler(log_name, encoding="utf-8")
        except FileNotFoundError:
            os.mkdir(log_path)
            fh = logging.FileHandler(log_name, encoding="utf-8")
        fh.setLevel(logging.DEBUG)  # 设置 file handler 的等级

        ch = logging.StreamHandler()  # 将日志同步输出到控制台
        ch.setLevel(logging.DEBUG)

        #  设置格式的输出

        formatter = logging.Formatter("%(asctime)s-%(name)s-%(levelname)s-%(message)s")  # 设置日志的格式
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        #  在实例中添加上两种输出方式
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)


if __name__ == '__main__':
    Logger("api").logger.debug("测试日志体系构建")
